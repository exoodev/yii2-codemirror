<?php

namespace exoo\codemirror;

use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;

/**
 * Codemirror HTML editor
 *
 * For example to use the timepicker with a [[\yii\base\Model|model]]:
 *
 * ```php
 * echo Codemirror::widget([
 *     'model' => $model,
 *     'attribute' => 'text',
 * ]);
 * ```
 *
 * The following example will use the name property instead:
 *
 * ```php
 * echo Codemirror::widget([
 *     'name'  => 'text',
 *     'value'  => $value,
 * ]);
 * ```
 *
 * You can also use this widget in an [[\yii\widgets\ActiveForm|ActiveForm]] using the [[\yii\widgets\ActiveField::widget()|widget()]]
 * method, for example like this:
 *
 * ```php
 * <?= $form->field($model, 'text')->widget(Codemirror::classname(), [
 *     'clientOptions' => ['lineNumbers' => true],
 * ]) ?>
 * ```
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Codemirror extends InputWidget
{
    /**
     * @var array the container options
     */
    public $containerOptions = ['class' => 'uk-card uk-card-default uk-card-body'];
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        Html::addCssClass($this->options, 'ex-codemirror');
        $defaults = ['dragDrop' => true];
        $clientOptions = Json::encode(array_replace_recursive($defaults, $this->clientOptions));
        $id = $this->options['id'];
        $view = $this->getView();
        $view->registerJs("CodeMirror.fromTextArea(document.getElementById('$id'), $clientOptions);");
        CodemirrorAsset::register($view);
    }

    /**
     * @inheritdoc
     */
    public function run()
	{
        if ($this->hasModel()) {
            $textarea = Html::activeTextarea($this->model, $this->attribute, $this->options);
        } else {
            $textarea = Html::textarea($this->name, $this->value, $this->options);
        }
        $tag = ArrayHelper::remove($this->containerOptions, 'tag', 'div');
        return Html::tag($tag, $textarea, $this->containerOptions);
	}
}
