<?php

namespace exoo\codemirror;

/**
 * Asset bundle for widget [[Codemirror]].
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class CodemirrorAsset extends \yii\web\AssetBundle
{
    /**
     * {@inheritdoc}
     */
    public $sourcePath = '@npm/codemirror';
    /**
     * {@inheritdoc}
     */
    public $css = [
        'lib/codemirror.css',
    ];
    /**
     * {@inheritdoc}
     */
    public $js = [
        'lib/codemirror.js',
        'mode/markdown/markdown.js',
        'addon/mode/overlay.js',
        'addon/edit/closetag.js',
        'addon/fold/xml-fold.js',
        'mode/xml/xml.js',
        'mode/gfm/gfm.js',
        'mode/htmlmixed/htmlmixed.js',
        'mode/javascript/javascript.js',
        'mode/css/css.js'
    ];
    /**
     * {@inheritdoc}
     */
    public $depends = [
        'yii\jui\JuiAsset',
    ];
}
